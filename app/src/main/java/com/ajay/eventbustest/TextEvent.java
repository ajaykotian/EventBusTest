package com.ajay.eventbustest;

/**
 * Created by ajaykotian on 10/11/17.
 */

public class TextEvent {

    private String textMessage;

    public String getTextMessage() {
        return textMessage;
    }

    public void setTextMessage(String textMessage) {
        this.textMessage = textMessage;
    }
}
