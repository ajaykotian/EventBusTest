package com.ajay.eventbustest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    Random random = new Random();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        EventBus.getDefault().register(this);
        Button button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < 250; i++) {
                    TextEvent textEvent = new TextEvent();
                    textEvent.setTextMessage("TEST MESSAGE " + i);
                    EventBus.getDefault().post(textEvent);
                }
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void onReceiveEvent(TextEvent event) {
        //((TextView) findViewById(R.id.textView)).setText(event.getTextMessage());
        Log.e("Message: ",event.getTextMessage());
    }
}
